import Header from './components/Header';
import Routes from './routes';
import GlobalStyle from './styles/global';

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
       <Header/>
       <Routes/>
       <GlobalStyle/>
      </header>
    </div>
  );
}

export default App;
