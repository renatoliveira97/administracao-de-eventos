import './style.css';

import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <header className="cabecalho">
            <div>
                <h2>Eventos</h2>
                <ul className="menu">
                    <li><Link className="link" to='/'>Home</Link></li>
                    <li><Link className="link" to='/formatura'>Formatura</Link></li>
                    <li><Link className="link" to='/casamento'>Casamento</Link></li>
                    <li><Link className="link" to='/confraternizacao'>Confraternização</Link></li>
                </ul>
            </div>
            <hr></hr>
        </header>
    );
}

export default Header;