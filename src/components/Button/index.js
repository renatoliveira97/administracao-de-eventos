import styled from "styled-components";

const Button = styled.button`
    background-color: #8E44AD;
    margin-bottom: 10px;
    width: 100%;
    height: 30px;
    color: #FFFFFF;
    border-radius: 10px;
    border: 1px solid rgba(0, 0, 0, 0);
    box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5);
`

export default Button;