import styled from "styled-components";

const Card = styled.div`
    width: 300px;
    background-color: #FFF;
    border-radius: 10px;
    margin: 10px;
    padding: 10px;
    box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.2);
`

export default Card;