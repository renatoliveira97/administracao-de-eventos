import './style.css';

import { useContext } from 'react';
import { BeersContext } from '../../Providers/beers';
import { BeerContext } from '../../Providers/beer';

import Card from '../../components/Card';
import Button from '../../components/Button';

const Home = () => {

    const { beers } = useContext(BeersContext);
    const { beer, setBeer, beerFormatura, setBeerFormatura, beerCasamento, setBeerCasamento, beerConfraternizacao, setBeerConfraternizacao } = useContext(BeerContext);

    const handleSelectBeer = (drink) => {
        setBeer(drink);
    }

    return (
        <div className="home-container">
            <div className="beers-container">
                {beers.map((beer) => (
                    <Card className="home-card" key={beer.id}>
                        <div>
                            <figure className='beer-figure'>
                                <img className='beer-image' src={beer.image_url} alt={beer.name}/>
                            </figure>
                            <hr></hr>
                            <h3>{beer.name}</h3>
                            <p>Inicio de fabricação: {beer.first_brewed}</p>
                            <p className='descricao'>{beer.description}</p>
                            <p>{beer.volume.value} litros</p>
                        </div>                    
                        <Button className='botao-home' onClick={() => handleSelectBeer(beer)}>Selecionar</Button>
                    </Card>
                ))}
            </div>
            <div className='beer-container'>
                {!beer ? (
                    <h3>Nenhuma bebida selecionada</h3>
                ) : (
                    <div>
                        <h3>{beer.name}</h3>
                        <Button className='botao-home' onClick={() => setBeerFormatura([...beerFormatura, beer])}>Formatura</Button>
                        <Button className='botao-home' onClick={() => setBeerCasamento([...beerCasamento, beer])}>Casamento</Button>
                        <Button className='botao-home' onClick={() => setBeerConfraternizacao([...beerConfraternizacao, beer])}>Confraternização</Button>
                    </div>
                )}
            </div>
        </div>
    );
}

export default Home;