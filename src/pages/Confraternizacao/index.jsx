import './style.css';

import { useContext } from 'react';
import { BeerContext } from '../../Providers/beer';

import Card from '../../components/Card';
import Button from '../../components/Button';

const Confraternizacao = () => {
    const { beerConfraternizacao, setBeerConfraternizacao } = useContext(BeerContext);

    const handleRemove = (id) => {
        const newList = beerConfraternizacao.filter((item) => item.id !== id);
        setBeerConfraternizacao(newList);
    }

    return (
        <div className="confraternizacao-container">
            {beerConfraternizacao.length === 0 ? (
                <h2>Nenhum item foi adicionado a lista! Por favor retorne a página inicial para adicionar items ao evento.</h2>
            ) : (
                beerConfraternizacao.map((beer) => (
                    <Card className="confraternizacao-card" key={beer.id}>
                        <div>
                            <figure className='beer-figure'>
                                <img className='beer-image' src={beer.image_url} alt={beer.name}/>
                            </figure>
                            <hr></hr>
                            <h3>{beer.name}</h3>
                            <p>Inicio de fabricação: {beer.first_brewed}</p>
                            <p className='descricao'>{beer.description}</p>
                            <p>{beer.volume.value} litros</p>
                        </div>                    
                        <Button className='botao-home' onClick={() => handleRemove(beer.id)}>Remover</Button>
                    </Card>
                ))
            )}            
        </div>
    );    
}

export default Confraternizacao;