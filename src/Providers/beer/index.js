import { createContext, useState } from 'react';

export const BeerContext = createContext();

export const BeerProvider = ({children}) => {
    const [beer, setBeer] = useState();
    const [beerFormatura, setBeerFormatura] = useState([]);
    const [beerCasamento, setBeerCasamento] = useState([]);
    const [beerConfraternizacao, setBeerConfraternizacao] = useState([]);

    return (
        <BeerContext.Provider value={{beer, setBeer, beerFormatura, setBeerFormatura, beerCasamento, setBeerCasamento, beerConfraternizacao, setBeerConfraternizacao}}>
            {children}
        </BeerContext.Provider>
    );
}