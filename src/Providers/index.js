import { BeersProvider } from './beers';
import { BeerProvider } from './beer';

const Providers = ({children}) => {
    return  <BeersProvider>
                <BeerProvider>{children}</BeerProvider>
            </BeersProvider>
}

export default Providers;