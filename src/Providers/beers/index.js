import { createContext, useState, useEffect } from 'react';

import axios from 'axios';

export const BeersContext = createContext();

export const BeersProvider = ({children}) => {
    const [beers, setBeers] = useState([]);

    const getBeers = () => {
        axios
            .get('https://api.punkapi.com/v2/beers')
            .then((response) => setBeers(response.data));
    }

    useEffect(() => {
        getBeers();
    }, []);

    return (
        <BeersContext.Provider value={{beers}}>
            {children}
        </BeersContext.Provider>
    );
}