import { Switch, Route } from 'react-router-dom';

import Home from '../pages/Home';
import Formatura from '../pages/Formatura';
import Casamento from '../pages/Casamento';
import Confraternizacao from '../pages/Confraternizacao';

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/formatura" component={Formatura}/>
            <Route path="/casamento" component={Casamento}/>
            <Route path="/confraternizacao" component={Confraternizacao}/>
        </Switch>
    )
}

export default Routes;