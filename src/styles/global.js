import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
        background-color: #EAF2F8;
        font-family: Montserrat, Sans-Serif;
    }
`

export default GlobalStyle;